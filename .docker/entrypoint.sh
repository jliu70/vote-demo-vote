#!/bin/bash
echo "---- Starting Up Service ---- "


if [ "$RANCHER_SERVICE_DISCOVERY" ]; then
  sleep 1

  function get_service {
    curl -sS --header "accept: application/json" http://rancher-metadata.rancher.internal/latest/$1
  }

  env=$( get_service "self/stack" )
  redis=$( get_service "services/redis" )

  IFS='-' read -ra NAMES <<< $( echo $env | jq -r '.environment_name' )
  env_vnet=$( echo ${NAMES[0]} )
  env_instance=$( echo ${NAMES[1]} )
  env_name=$( echo $env | jq -r '.name')

  # setup all of the environment variable
  if [ -z "$AEL_ENVIRONMENT" ]; then
    export AEL_ENVIRONMENT=$( echo $env_instance )
  fi

  if [ -z "$REDIS_HOST" ]; then
    #export REDIS_HOST=redis.redis.rancher.internal
    #export REDIS_HOST=$( echo $redis | jq -r '.containers[0].labels["io.rancher.project_service.name"]' | sed -e 's!/!.!' )
    # the above does not work as the project_name service_name order does not match DNS
    stackName=$( echo $redis | jq -r '.containers[0].stack_name' )
    serviceName=$( echo $redis | jq -r '.containers[0].service_name' )
    export REDIS_HOST=$( echo ${serviceName}.${stackName} )
  fi
  if [ -z "$REDIS_PORT" ]; then
    export REDIS_PORT=$( echo $redis | jq -r .ports[0] | sed -e 's/\/\(tcp\|udp\)//' | cut -d: -f1 )
  fi
  if [ -z "$REDIS_USERNAME" ]; then
    export REDIS_USERNAME=$( echo $redis | jq -r '.labels["io.ael.service.username"]' )
  fi
  if [ -z "$REDIS_PASSWORD" ]; then
    export REDIS_PASSWORD=$( echo $redis | jq -r '.labels["io.ael.service.password"]' )
  fi
  if [ -z "$REDIS_VHOST" ]; then
    export REDIS_VHOST=$( echo $redis | jq -r '.labels["io.ael.service.vhost"]' )
  fi

fi

# print out all of the environment variables
echo "#### entry.sh - Start Dump Variables ####"
env
echo "#### entry.sh - End Dump Variables ####"

# Execute the commands passed to this script
exec "$@"
