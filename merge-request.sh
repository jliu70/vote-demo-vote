GITLABHOST=$(echo ${CI_PROJECT_URL} | awk -F/ '{print $3}' )
COMMITHASH=${CI_BUILD_REF:0:8}

if [ $1 ]; then
  export REF_NAME="$1"
else
  export REF_NAME="${CI_BUILD_REF_NAME}"
fi

if [ "$REF_NAME" = "develop" ]; then
  export TARGET_REF_NAME="master"
else
  export TARGET_REF_NAME="develop"
fi

echo "REF_NAME is $REF_NAME"
echo "TARGET_REF_NAME is $TARGET_REF_NAME"

# Submit Merge Request
# https://nacho4d-nacho4d.blogspot.ch/2016/05/gitlab-merge-request-from-terminal.html
echo curl --header "PRIVATE-TOKEN: ${BUILDBOTTOKEN}" -X POST "https://${GITLABHOST}/api/v3/projects/${CI_PROJECT_ID}/merge_requests" --data "source_branch=${REF_NAME}" --data "target_branch=${TARGET_REF_NAME}" --data "title=merge request from cli api for ${CI_PROJECT_NAME} ${REF_NAME}_${APP_VERSION} ${COMMITHASH}"
curl --header "PRIVATE-TOKEN: ${BUILDBOTTOKEN}" -X POST "https://${GITLABHOST}/api/v3/projects/${CI_PROJECT_ID}/merge_requests" --data "source_branch=${REF_NAME}" --data "target_branch=${TARGET_REF_NAME}" --data "title=merge request from cli api for ${CI_PROJECT_NAME} ${REF_NAME} ${APP_VERSION} ${COMMITHASH}"


# List merge requests - grab the id of the latest MR
#   NOTE: future enhancement: implement some checks here to verify the MR by title or some other artifact
# http://docs.gitlab.com/ce/api/merge_requests.html#list-merge-requests
# 
# GET /projects/:id/merge_requests?state=opened
#curl --header "PRIVATE-TOKEN: ${BUILDBOTTOKEN}" "https://${GITLABHOST}/api/v3/projects/${CI_PROJECT_ID}/merge_requests?state=opened" | jq .
id=$(curl --header "PRIVATE-TOKEN: ${BUILDBOTTOKEN}" "https://${GITLABHOST}/api/v3/projects/${CI_PROJECT_ID}/merge_requests?state=opened" | jq '.[0] | { id }' | grep id | cut -d: -f 2  | sed -e 's/ //g')
echo "id is $id"


# Get single MR 
# GET /projects/:id/merge_requests/:merge_request_id
#echo curl --header "PRIVATE-TOKEN: ${BUILDBOTTOKEN}" "https://${GITLABHOST}/api/v3/projects/${CI_PROJECT_ID}/merge_requests/${id}" 
#curl --header "PRIVATE-TOKEN: ${BUILDBOTTOKEN}" "https://${GITLABHOST}/api/v3/projects/${CI_PROJECT_ID}/merge_requests/${id}"  | jq .


#  Accept Merge Request
# http://docs.gitlab.com/ce/api/merge_requests.html#accept-mr
# PUT /projects/:id/merge_requests/:merge_request_id/merge
# http://stackoverflow.com/questions/38121378/gitlab-api-merge-when-build-succeeds
#echo curl --header "PRIVATE-TOKEN: ${BUILDBOTTOKEN}" -d merge_when_build_succeeds=true -X PUT "https://${GITLABHOST}/api/v3/projects/${CI_PROJECT_ID}/merge_requests/${id}/merge" --data "merge_when_build_succeeds=true"
#curl --header "PRIVATE-TOKEN: ${BUILDBOTTOKEN}" -d merge_when_build_succeeds=true -X PUT "https://${GITLABHOST}/api/v3/projects/${CI_PROJECT_ID}/merge_requests/${id}/merge" 
